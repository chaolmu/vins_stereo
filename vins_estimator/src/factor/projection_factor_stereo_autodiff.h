#pragma once

#include <ros/assert.h>
#include <ceres/ceres.h>
#include <Eigen/Dense>
#include "../utility/utility.h"
#include "../utility/tic_toc.h"
#include "../parameters.h"

#include <ceres/internal/autodiff.h>

class ProjectionStereoAutoDiff : public ceres::SizedCostFunction<2, 7, 7, 7, 7, 1>
{
public:
  ProjectionStereoAutoDiff(const Eigen::Vector3d &_pts_i, const Eigen::Vector3d &_pts_j) : pts_i(_pts_i), pts_j(_pts_j)
  {
  }

  template <typename T>
  bool operator()(const T *_pose_i, const T *_pose_j,
                  const T *_ex_pose, const T *_ex_pose1,
                  const T *_inv_dep, T *residuals) const
  {
    Eigen::Map<const Eigen::Matrix<T, 3, 1>> Pi(_pose_i);
    Eigen::Map<const Eigen::Matrix<T, 3, 1>> aa_Qi(_pose_i + 3);
    Eigen::AngleAxis<T> Qi(aa_Qi.norm(), aa_Qi / aa_Qi.norm());
    // Eigen::Quaternion<T> Qi((T)Qi_.w(), (T)Qi_.z(), (T)Qi_.y(), (T)Qi_.z());

    Eigen::Map<const Eigen::Matrix<T, 3, 1>> Pj(_pose_j);
    Eigen::Map<const Eigen::Matrix<T, 3, 1>> aa_Qj(_pose_j + 3);
    Eigen::AngleAxis<T> Qj(aa_Qj.norm(), aa_Qj / aa_Qj.norm());
    // Eigen::Quaternion<T> Qj((T)Qj_.w(), (T)Qj_.z(), (T)Qj_.y(), (T)Qj_.z());

    Eigen::Map<const Eigen::Matrix<T, 3, 1>> tic(_ex_pose);
    Eigen::Map<const Eigen::Matrix<T, 3, 1>> aa_qic(_ex_pose + 3);
    Eigen::AngleAxis<T> qic(aa_qic.norm(), aa_qic / aa_qic.norm());
    // Eigen::Quaternion<T> qic((T)qic_.w(), (T)qic_.z(), (T)qic_.y(), (T)qic_.z());

    Eigen::Map<const Eigen::Matrix<T, 3, 1>> tic1(_ex_pose1);
    Eigen::Map<const Eigen::Matrix<T, 3, 1>> aa_qic1(_ex_pose1 + 3);
    Eigen::AngleAxis<T> qic1(aa_qic1.norm(), aa_qic1 / aa_qic1.norm());
    // Eigen::Quaternion<T> qic1((T)qic1_.w(), (T)qic1_.z(), (T)qic1_.y(), (T)qic1_.z());

    T inv_dep_i = _inv_dep[0];

    Eigen::Matrix<T, 3, 1> pts_ci_meas((T)pts_i(0), (T)pts_i(1), (T)pts_i(2));
    Eigen::Matrix<T, 3, 1> pts_cj_meas((T)pts_j(0), (T)pts_j(1), (T)pts_j(2));

    Eigen::Matrix<T, 3, 1> pts_ci = pts_ci_meas / inv_dep_i;
    Eigen::Matrix<T, 3, 1> pts_imu_i = qic * pts_ci + tic;
    Eigen::Matrix<T, 3, 1> pts_w = Qi * pts_imu_i + Pi;
    Eigen::Matrix<T, 3, 1> pts_imu_j = Qj.inverse() * (pts_w - Pj);
    Eigen::Matrix<T, 3, 1> pts_cj = qic1.inverse() * (pts_imu_j - tic1);

    pts_cj = pts_cj / pts_cj(2);
    Eigen::Matrix<T, 3, 1> residual = pts_cj - pts_cj_meas;

    residuals[0] = (T)(360.0 / 1.5) * residual(0);
    residuals[1] = (T)(360.0 / 1.5) * residual(1);

    return true;
  }

  virtual bool Evaluate(double const *const *parameters, double *residuals, double **jacobians) const
  {
    Eigen::Map<const Eigen::Vector3d> Pi(parameters[0]);
    Eigen::Map<const Eigen::Quaterniond> Qi(parameters[0] + 3);

    Eigen::Map<const Eigen::Vector3d> Pj(parameters[1]);
    Eigen::Map<const Eigen::Quaterniond> Qj(parameters[1] + 3);

    Eigen::Map<const Eigen::Vector3d> tic(parameters[2]);
    Eigen::Map<const Eigen::Quaterniond> qic(parameters[2] + 3);

    Eigen::Map<const Eigen::Vector3d> tic1(parameters[3]);
    Eigen::Map<const Eigen::Quaterniond> qic1(parameters[3] + 3);

    double inv_dep_i = parameters[4][0];


    // Qi_ = Eigen::Quaterniond(Qi);
    // Qi_ = Eigen::Quaterniond(Qi.w(), Qi.x(), Qi.y(), Qi.z());
    // Qj_ = Eigen::Quaterniond(Qj.w(), Qj.x(), Qj.y(), Qj.z());
    // qic_ = Eigen::Quaterniond(qic.w(), qic.x(), qic.y(), qic.z());
    // qic1_ = Eigen::Quaterniond(qic1.w(), qic1.x(), qic1.y(), qic1.z());

    Eigen::AngleAxisd aa_Qi(Qi);
    Eigen::AngleAxisd aa_Qj(Qj);
    Eigen::AngleAxisd aa_qic(qic);
    Eigen::AngleAxisd aa_qic1(qic1);

    Eigen::Vector3d theta_Qi = aa_Qi.angle() * aa_Qi.axis();
    Eigen::Vector3d theta_Qj = aa_Qj.angle() * aa_Qj.axis();
    Eigen::Vector3d theta_qic = aa_qic.angle() * aa_qic.axis();
    Eigen::Vector3d theta_qic1 = aa_qic1.angle() * aa_qic1.axis();

    // theta_Qi.setZero();
    // theta_Qj.setZero();
    // theta_qic.setZero();
    // theta_qic1.setZero();

    Eigen::Matrix<double, 6, 1> para_block1;
    para_block1 << Pi, theta_Qi;

    Eigen::Matrix<double, 6, 1> para_block2;
    para_block2 << Pj, theta_Qj;

    Eigen::Matrix<double, 6, 1> para_block3;
    para_block3 << tic, theta_qic;

    Eigen::Matrix<double, 6, 1> para_block4;
    para_block4 << tic1, theta_qic1;

    Eigen::Matrix<double, 1, 1> para_block5;
    para_block5 << inv_dep_i;

    const double *paras[] = {para_block1.data(), para_block2.data(),
                             para_block3.data(), para_block4.data(), para_block5.data()};

    double **jacobs = new double *[5];
    jacobs[0] = new double[2 * 6];
    jacobs[1] = new double[2 * 6];
    jacobs[2] = new double[2 * 6];
    jacobs[3] = new double[2 * 6];
    jacobs[4] = new double[2 * 1];

    typedef ceres::internal::AutoDiff<ProjectionStereoAutoDiff, double, 6, 6, 6, 6, 1> AutoDiffType;
    bool diffState = AutoDiffType::Differentiate(*this, paras, 2, residuals, jacobs);

    if (jacobians)
    {
      // if(diffState)
      // {
      Eigen::Map<Eigen::Matrix<double, 2, 6, Eigen::RowMajor>> J_i(jacobs[0]);
      Eigen::Map<Eigen::Matrix<double, 2, 6, Eigen::RowMajor>> J_j(jacobs[1]);
      Eigen::Map<Eigen::Matrix<double, 2, 6, Eigen::RowMajor>> J_ic(jacobs[2]);
      Eigen::Map<Eigen::Matrix<double, 2, 6, Eigen::RowMajor>> J_ic1(jacobs[3]);
      Eigen::Map<Eigen::Matrix<double, 2, 1>> J_dep(jacobs[4]);

      // cout << "J_i: " << endl
      //  << Eigen::Map<Eigen::Matrix<double,1,12>, Eigen::RowMajor>(jacobs[0]) << endl;

      // cout << "J_i: " << endl
      //  << Eigen::Map<Eigen::Matrix<double,2,6>, Eigen::RowMajor>(jacobs[0]) << endl;

      // cout << "J_i: " << endl
      //  << Eigen::Map<Eigen::Matrix<double,2,6>, Eigen::ColMajor>(jacobs[0]) << endl;

      if (jacobians[0])
      {
        Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacob_i(jacobians[0]);
        jacob_i.leftCols<6>() = J_i;
        jacob_i.rightCols<1>().setZero();
        // cout << "J_i1:\n"
        //      << jacob_i.leftCols<6>() << endl;
        // cout << "J_i2:\n"
        //      << jacob_i.rightCols<1>() << endl;
        // cout << "J_i3:\n"
        //      << jacob_i << endl;

        // cout << "jacobians[0]" << endl;
        // for (int i = 0; i < 14; i++)
        //   cout << jacobians[0][i] << ",";
        // cout << endl;
      }

      if (jacobians[1])
      {
        Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacob_j(jacobians[1]);
        jacob_j.leftCols<6>() = J_j;
        jacob_j.rightCols<1>().setZero();
      }

      if (jacobians[2])
      {
        Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacob_ic(jacobians[2]);
        jacob_ic.leftCols<6>() = J_ic;
        jacob_ic.rightCols<1>().setZero();
      }

      if (jacobians[3])
      {
        Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacob_ic1(jacobians[3]);
        jacob_ic1.leftCols<6>() = J_ic1;
        jacob_ic1.rightCols<1>().setZero();
      }

      if (jacobians[4])
      {
        Eigen::Map<Eigen::Matrix<double, 2, 1>> jacob_dep(jacobians[4]);
        jacob_dep = J_dep;
      }
    }
    // }
    // else
    // {
    //   assert ( 0 && "Error while differentiating" );
    // }

    return true;
  }

  void check(double **parameters)
  {
  }

  Eigen::Vector3d pts_i, pts_j;
  static Eigen::Matrix2d sqrt_info;
  Eigen::Quaterniond Qi_,Qj_,qic_,qic1_;
};

// struct ProjectionStereoFunctor
// {
//   Eigen::Vector3d pts_i, pts_j;

//   ProjectionStereoFunctor(const Eigen::Vector3d &_pts_i, const Eigen::Vector3d &_pts_j):
//                           pts_i(_pts_i), pts_j(_pts_j)
//   {

//   }

//   template <typename T>
//   bool operator()(const T* _pose_i, const T* _pose_j,
//                   const T* _ex_pose, const T* _ex_pose1,
//                   const T* _inv_dep, T* residuals) const
//   {
//     Eigen::Map<const Eigen::Matrix<T,3,1>, Eigen::RowMajor> Pi(_pose_i);
//     Eigen::Map<const Eigen::AngleAxis<T>> Qi(_pose_i[3], _pose_i[4], _pose_i[5]);

//     Eigen::Map<const Eigen::Matrix<T,3,1>, Eigen::RowMajor> Pj(_pose_j);
//     Eigen::Map<const Eigen::AngleAxis<T>> Qj(_pose_j[3], _pose_j[4], _pose_j[5]);

//     Eigen::Map<const Eigen::Matrix<T,3,1>, Eigen::RowMajor> tic(_ex_pose);
//     Eigen::Map<const Eigen::AngleAxis<T>> qic(_ex_pose[3], _ex_pose[4], _ex_pose[5]);

//     Eigen::Map<const Eigen::Matrix<T,3,1>, Eigen::RowMajor> tic1(_ex_pose1);
//     Eigen::Map<const Eigen::AngleAxis<T>> qic1(_ex_pose1[3], _ex_pose1[4], _ex_pose1[5]);

//     T inv_dep_i = _inv_dep[0];

//     Eigen::Matrix<T,3,1> pts_ci_meas((T)pts_i(0), (T)pts_i(1), (T)pts_i(2));
//     Eigen::Matrix<T,3,1> pts_cj_meas((T)pts_j(0), (T)pts_j(1), (T)pts_j(2));

//     Eigen::Matrix<T,3,1> pts_ci = pts_ci_meas / inv_dep_i;
//     Eigen::Matrix<T,3,1> pts_imu_i = qic * pts_ci + tic;
//     Eigen::Matrix<T,3,1> pts_w = Qi * pts_imu_i + Pi;
//     Eigen::Matrix<T,3,1> pts_imu_j = Qj.inverse() * (pts_w - Pj);
//     Eigen::Matrix<T,3,1> pts_cj = qic1.inverse() * (pts_imu_j - tic1);

//     pts_cj = pts_cj/pts_cj(2);
//     Eigen::Matrix<T,3,1> residual = pts_cj - pts_cj_meas;

//     residuals[0] = (T)(360.0/1.5) * residual(0);
//     residuals[1] = (T)(360.0/1.5) * residual(1);

//     return true;
//   }
// };

// struct ProjectionStereoAutoDiff
// {
//   ProjectionStereoAutoDiff(const Eigen::Vector3d &_pts_i, const Eigen::Vector3d &_pts_j):
//                           pts_i(_pts_i), pts_j(_pts_j)
//   {

//   }

//   template <typename T>
//   bool operator()(const T* _pose_i, const T* _pose_j,
//                   const T* _ex_pose, const T* _ex_pose1,
//                   const T* _inv_dep, T* residuals) const
//   {
//     // T Pi[3] = {_pose_i[0], _pose_i[1], _pose_i[2]};
//     // T Qi[4] = {_pose_i[6], _pose_i[3], _pose_i[4], _pose_i[5]};
//     // T Pj[3] = {_pose_j[0], _pose_j[1], _pose_j[2]};
//     // T Qj[4] = {_pose_j[6], _pose_j[3], _pose_j[4], _pose_j[5]};
//     // T tic[3] = {_ex_pose[0], _ex_pose[1], _ex_pose[2]};
//     // T qic[4] = {_ex_pose[6], _ex_pose[3], _ex_pose[4], _ex_pose[5]};
//     // T tic1[3] = {_ex_pose1[0], _ex_pose1[1], _ex_pose1[2]};
//     // T qic1[4] = {_ex_pose1[6], _ex_pose1[3], _ex_pose1[4], _ex_pose1[5]};

//     // T inv_dep_i = _inv_dep[0];

//     // T pts_ci_meas[3] = {(T)pts_i(0), (T)pts_i(1), (T)pts_i(2)};
//     // T pts_cj_meas[3] = {(T)pts_j(0), (T)pts_j(1), (T)pts_j(2)};

//     // T pts_ci[3] = {pts_ci_meas[0]/inv_dep_i, pts_ci_meas[1]/inv_dep_i, pts_ci_meas[2]/inv_dep_i};

//     // T pts_imu_i[3];
//     // ceres::QuaternionRotatePoint(qic, pts_ci, pts_imu_i);
//     // pts_imu_i[0] += tic[0]; pts_imu_i[1] += tic[1]; pts_imu_i[2] += tic[2];

//     // T pts_w[3];
//     // ceres::QuaternionRotatePoint(Qi, pts_imu_i, pts_w);
//     // pts_w[0] += Pi[0]; pts_w[1] += Pi[1]; pts_w[2] += Pi[2];

//     // T pts_imu_j[3];
//     // T Qj_inv[4] = {Qj[0], -Qj[1], -Qj[2], -Qj[3]};
//     // T tmp[3];
//     // tmp[0] = pts_w[0] - Pj[0]; tmp[1] = pts_w[1] - Pj[1]; tmp[2] = pts_w[2] - Pj[2];
//     // ceres::QuaternionRotatePoint(Qj_inv, tmp, pts_imu_j);

//     // tmp[0] = pts_imu_j[0] - tic1[0]; tmp[1] = pts_imu_j[1] - tic1[1]; tmp[2] = pts_imu_j[2] - tic1[2];
//     // T qic1_inv[4] = {qic1[0], -qic1[1], -qic1[2], -qic1[3]};
//     // T pts_cj[3];
//     // ceres::QuaternionRotatePoint(qic1_inv, tmp, pts_cj);

//     // pts_cj[0] /= pts_cj[2];
//     // pts_cj[1] /= pts_cj[2];

//     // T residual[2] = {pts_cj[0] - pts_cj_meas[0], pts_cj[1] - pts_cj_meas[1]};

//     // residuals[0] = (T)sqrt_info(0,0)*residual[0] + (T)sqrt_info(0,1)*residual[1];
//     // residuals[1] = (T)sqrt_info(1,0)*residual[0] + (T)sqrt_info(1,1)*residual[1];

//     Eigen::Map<const Eigen::Matrix<T,3,1>, Eigen::RowMajor> Pi(_pose_i);
//     Eigen::Quaternion<T> Qi(_pose_i[6], _pose_i[3], _pose_i[4], _pose_i[5]);
//     // Qi.normalize();

//     Eigen::Map<const Eigen::Matrix<T,3,1>, Eigen::RowMajor> Pj(_pose_j);
//     Eigen::Quaternion<T> Qj(_pose_j[6], _pose_j[3], _pose_j[4], _pose_j[5]);
//     // Qj.normalize();

//     Eigen::Map<const Eigen::Matrix<T,3,1>, Eigen::RowMajor> tic(_ex_pose);
//     Eigen::Quaternion<T> qic(_ex_pose[6], _ex_pose[3], _ex_pose[4], _ex_pose[5]);
//     // qic.normalize();

//     Eigen::Map<const Eigen::Matrix<T,3,1>, Eigen::RowMajor> tic1(_ex_pose1);
//     Eigen::Quaternion<T> qic1(_ex_pose1[6], _ex_pose1[3], _ex_pose1[4], _ex_pose1[5]);
//     // qic1.normalize();

//     T inv_dep_i = _inv_dep[0];

//     Eigen::Matrix<T,3,1> pts_ci_meas((T)pts_i(0), (T)pts_i(1), (T)pts_i(2));
//     Eigen::Matrix<T,3,1> pts_cj_meas((T)pts_j(0), (T)pts_j(1), (T)pts_j(2));

//     Eigen::Matrix<T,3,1> pts_ci = pts_ci_meas / inv_dep_i;
//     Eigen::Matrix<T,3,1> pts_imu_i = qic * pts_ci + tic;
//     Eigen::Matrix<T,3,1> pts_w = Qi * pts_imu_i + Pi;
//     Eigen::Matrix<T,3,1> pts_imu_j = Qj.inverse() * (pts_w - Pj);
//     Eigen::Matrix<T,3,1> pts_cj = qic1.inverse() * (pts_imu_j - tic1);

//     pts_cj = pts_cj/pts_cj(2);
//     Eigen::Matrix<T,3,1> residual = pts_cj - pts_cj_meas;

//     residuals[0] = (T)sqrt_info(0,0) * residual(0);
//     residuals[1] = (T)sqrt_info(1,1) * residual(1);

//     // cout << "Qi w = " << Qi.w() << endl;
//     // cout << "Qi x = " << Qi.x() << endl;
//     // cout << "Qi y = " << Qi.y() << endl;
//     // cout << "Qi z = " << Qi.z() << endl;

//     // cout << "depth = " << 1.0/inv_dep_i << endl;

//     // cout << "sqrt_info = " << sqrt_info << endl;
//     // cout << "residual = " << residuals[0] << "," << residuals[1] << endl;

//     // printf("Pi[0] = %f\n",(double)inv_dep_i);

//     return true;
//   }

// 	static ceres::CostFunction* Create(const Eigen::Vector3d &pts_i, const Eigen::Vector3d &pts_j)
// 	{
// 	  return (new ceres::AutoDiffCostFunction<
// 	          ProjectionStereoAutoDiff, 2, 7, 7, 7, 7, 1>(
// 	          	new ProjectionStereoAutoDiff(pts_i,pts_j)));
// 	}

//   Eigen::Vector3d pts_i, pts_j;
//   static Eigen::Matrix2d sqrt_info;

// };
