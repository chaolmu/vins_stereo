# VINS-Stereo
Stereo extension of VINS-Mono(https://github.com/HKUST-Aerial-Robotics/VINS-Mono).


<!-- <img src="./support/large_scale_hohe_str_dd.png"  height ="250"> -->
<img src="./support/vins-stereo-hohe-str.gif"  height="250">
<img src="./support/google-map-vins.png"  height ="250">

 Video: [Youtube](https://www.youtube.com/watch?v=XW3dV7o6F24&t=39s)

## 1. Prerequisites
### 1.1 **Ubuntu** and **ROS**
- Ubuntu  16.04 or above. (Tested with Ubuntu 16.04 & 18.04)
- ROS Kinetic or above.

### 1.2 **Ceres-solver**  
Installtion guide: http://ceres-solver.org/installation.html.

```
# CMake
sudo apt-get install cmake
# google-glog + gflags
sudo apt-get install libgoogle-glog-dev
# BLAS & LAPACK
sudo apt-get install libatlas-base-dev
# Eigen3
sudo apt-get install libeigen3-dev
# SuiteSparse and CXSparse (optional)
sudo apt-get install libsuitesparse-dev

cd ~
git clone https://github.com/ceres-solver/ceres-solver
cd ceres-solver
mkdir build
cd ./build
cmake ..
make -j4
sudo make install
```

## 2. Build
```
cd ~
mkdir -p vins-stereo/src
cd vins-stereo
catkin_init_workspace src
cd src
git clone https://gitlab.com/FLYing-maniPULATOR/vins_stereo.git
cd ..
catkin_make
```
## 3. Run with EuRoC dataset
Download rosbag from https://projects.asl.ethz.ch/datasets/doku.php?id=kmavvisualinertialdatasets.  
Example:
```
cd ~/vins-stereo
source devel/setup.bash
roslaunch vins_estimator euroc.launch 
rosbag play PATH_TO_BAGFILE/NAME_OF_BAGFILE.bag
```
## 4. Run with MYNT EYE S
### 4.1 Build and run mynt eye wrapper
Make and install library:
```
cd ~
git clone https://github.com/slightech/MYNT-EYE-S-SDK.git
cd MYNT-EYE-S-SDK
make ros
```
Run camera:
```
cd wrappers/ros
source devel/setup.bash
roslaunch mynt_eye_ros_wrapper mynteye.launch
```

To change camera/IMU parameters (i.e. frame rate):  
edit the .yaml file:  `mynt_eye_ros_wrapper/config/device/standard.yaml`.  

For more details see MYNT EYE SDK: https://github.com/slightech/MYNT-EYE-S-SDK.git.
### 4.2 Run VINS-Stereo
```
roslaunch vins_estimator mynteye.launch
```

## 5. Configuration file  
Configurations and camera parameters are in folder `config/`.  
Currently supported camera distortion models: **radtan**, **equidistant**. 



