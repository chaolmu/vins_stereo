#pragma once

#include <cstdio>
#include <iostream>
#include <queue>
#include <execinfo.h>
#include <csignal>

#include <eigen3/Eigen/Dense>

#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

#include "camodocal/camera_models/CameraFactory.h"
#include "camodocal/camera_models/CataCamera.h"
#include "camodocal/camera_models/PinholeCamera.h"

#include "parameters.h"
#include "tic_toc.h"

using namespace std;
using namespace camodocal;
using namespace Eigen;

bool inBorder(const cv::Point2f &pt);

void reduceVector(vector<cv::Point2f> &v, vector<uchar> status);
void reduceVector(vector<int> &v, vector<uchar> status);

class FeatureTracker
{
public:
    FeatureTracker();

    class CameraModel
    {
    public:
        std::string model_name;
        cv::Vec4d K;
        cv::Vec4d D;
    };

    void readImage(const cv::Mat &_img0, const cv::Mat &_img1,
                   double _cur_time, const cv::Matx33f &c_R_p);

    void setMask();

    void addPoints();

    bool updateID(unsigned int i);

    void readIntrinsicParameter(const string &calib_file, camodocal::CameraPtr &camera);

    void checkBorder(const std::vector<cv::Point2f> &_pts, std::vector<unsigned char> &_status);

    void showUndistortionCam0(const string &name);

    void showUndistortionCam1(const string &name);

    void undistortedPoints();

    void predictTracking(const cv::Matx33f &c_R_p);

    static void triangulatePoint(const Eigen::Matrix<double, 3, 4> &Pose0, const Eigen::Matrix<double, 3, 4> &Pose1,
                                 const Vector2d &point0, const Vector2d &point1, Vector3d &point_3d);

    void stereoMatch(
        const std::vector<cv::Point2f> &_cam0_pts, const cv::Mat &_img0,
        std::vector<cv::Point2f> &_cam1_pts, const cv::Mat &_img1,
        std::vector<unsigned char> &_status);

    void showMatchImg(const std::vector<cv::Point2f> &p1, const cv::Mat &img1,
                      const std::vector<cv::Point2f> &p2, const cv::Mat &img2,
                      const std::vector<unsigned char> inlier_marker,
                      const std::string win_name = "Matches");

    void undistortPoints(
        const std::vector<cv::Point2f> &pts_in,
        const cv::Vec4d &intrinsics,
        const std::string &distortion_model,
        const cv::Vec4d &distortion_coeffs,
        std::vector<cv::Point2f> &pts_out,
        const cv::Matx33d &rectification_matrix = cv::Matx33d::eye(),
        const cv::Vec4d &new_intrinsics = cv::Vec4d(1, 1, 0, 0));

    vector<cv::Point2f> distortPoints(
        const vector<cv::Point2f> &pts_in,
        const cv::Vec4d &intrinsics,
        const string &distortion_model,
        const cv::Vec4d &distortion_coeffs);

    cv::Point2f distortPoint(
        const cv::Point2f &pt_in,
        const cv::Vec4d &intrinsics,
        const string &distortion_model,
        const cv::Vec4d &distortion_coeffs);

    void twoPointRansac(
        const vector<cv::Point2f> &pts1, const vector<cv::Point2f> &pts2,
        const cv::Matx33f &R_p_c, const cv::Vec4d &intrinsics,
        const std::string &distortion_model,
        const cv::Vec4d &distortion_coeffs,
        const double &inlier_error,
        const double &success_probability,
        vector<int> &inlier_markers);

    void rescalePoints(
        vector<cv::Point2f> &pts1, vector<cv::Point2f> &pts2,
        float &scaling_factor);

    cv::Mat mask;
    cv::Mat fisheye_mask;
    cv::Mat cur_img0, forw_img0;
    cv::Mat cur_img1, forw_img1;
    // new keypoints
    vector<cv::Point2f> new_pts_cam0;
    vector<cv::Point2f> new_pts_cam1;
    vector<cv::Point2f> cur_pts_cam0, forw_pts_cam0;
    vector<cv::Point2f> cur_pts_cam1, forw_pts_cam1;
    vector<cv::Point2f> prev_un_pts_cam0, cur_un_pts_cam0;
    vector<cv::Point2f> prev_un_pts_cam1, cur_un_pts_cam1;
    vector<cv::Point2f> pts_velocity_cam0, pts_velocity_cam1;
    vector<int> ids;
    vector<int> track_cnt;
    map<int, cv::Point2f> cur_un_pts_map; // for calculating point velocity
    map<int, cv::Point2f> prev_un_pts_map;
    camodocal::CameraPtr m_camera_0;
    camodocal::CameraPtr m_camera_1;
    double cur_time;
    double prev_time;

    static int n_id;

    // debug and develop only
    // cv::Vec4d cam0_intrinsics;
    // cv::Vec4d cam1_intrinsics;
    // cv::Vec4d cam0_distort_coeffs;
    // cv::Vec4d cam1_distort_coeffs;

    CameraModel model_cam0;
    CameraModel model_cam1;

    cv::Matx33d imu_R_cam0;
    cv::Matx33d imu_R_cam1;
    cv::Vec3d imu_t_cam0;
    cv::Vec3d imu_t_cam1;
};
