#pragma once
#include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>

extern int ROW;
extern int COL;
extern int FOCAL_LENGTH;
const int NUM_OF_CAM = 1;


extern std::string IMAGE_TOPIC;
extern std::string IMAGE_TOPIC_R;
extern std::string IMU_TOPIC;
extern std::string FISHEYE_MASK;
extern std::string CAM0_MODEL_PATH;
extern std::string CAM1_MODEL_PATH; 
extern std::string CAM0_MODEL_NAME; 
extern std::string CAM1_MODEL_NAME;
extern cv::Mat IMU_T_CAM0; // extrinsic of cam0, transform w.r.t. IMU-frame
extern cv::Mat IMU_T_CAM1; // extrinsic of cam1, transform w.r.t. IMU-frame
extern cv::Mat CAM0_MODEL_K; // intrinsics
extern cv::Mat CAM1_MODEL_K;
extern cv::Mat CAM0_MODEL_D; // distortion coeffcients
extern cv::Mat CAM1_MODEL_D;
extern std::vector<std::string> CAM_NAMES;
extern int MAX_CNT;
extern int MIN_DIST;
extern int WINDOW_SIZE;
extern int FREQ;
extern double F_THRESHOLD;
extern double STEREO_THRESHOLD;
extern int MIN_TRACKED_CNT;
extern int SHOW_TRACK;
extern int STEREO_TRACK;
extern int EQUALIZE;
extern int FISHEYE;
extern bool PUB_THIS_FRAME;

void readParameters(ros::NodeHandle &n);
