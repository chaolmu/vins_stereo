#include "parameters.h"

std::string IMAGE_TOPIC;
std::string IMAGE_TOPIC_R;
std::string IMU_TOPIC;
std::string CAM0_MODEL_PATH;
std::string CAM1_MODEL_PATH; 
std::string CAM0_MODEL_NAME; 
std::string CAM1_MODEL_NAME;
cv::Mat IMU_T_CAM0; // extrinsic of cam0, transform w.r.t. IMU-frame
cv::Mat IMU_T_CAM1; // extrinsic of cam1, transform w.r.t. IMU-frame
cv::Mat CAM0_MODEL_K; // intrinsics
cv::Mat CAM1_MODEL_K;
cv::Mat CAM0_MODEL_D; // distortion coeffcients
cv::Mat CAM1_MODEL_D;
std::vector<std::string> CAM_NAMES;
std::string FISHEYE_MASK;
int MAX_CNT;
int MIN_DIST;
int WINDOW_SIZE;
int FREQ;
double F_THRESHOLD;
double STEREO_THRESHOLD;
int MIN_TRACKED_CNT;
int SHOW_TRACK;
int STEREO_TRACK; // not used
int EQUALIZE;
int ROW;
int COL;
int FOCAL_LENGTH;
int FISHEYE;
bool PUB_THIS_FRAME;

template <typename T>
T readParam(ros::NodeHandle &n, std::string name)
{
    T ans;
    if (n.getParam(name, ans))
    {
        ROS_INFO_STREAM("Loaded " << name << ": " << ans);
    }
    else
    {
        ROS_ERROR_STREAM("Failed to load " << name);
        n.shutdown();
    }
    return ans;
}

void readParameters(ros::NodeHandle &n)
{
    std::string config_file;
    std::string config_path;

    config_file = readParam<std::string>(n, "config_file");
    config_path = config_file.substr(0, config_file.find_last_of('/'));
    cv::FileStorage fsSettings(config_file, cv::FileStorage::READ);
    if(!fsSettings.isOpened())
    {
        std::cerr << "ERROR: Wrong path to settings" << std::endl;
    }
    std::string VINS_FOLDER_PATH = readParam<std::string>(n, "vins_folder");

    fsSettings["image_topic"] >> IMAGE_TOPIC;
    fsSettings["image_topic_r"] >> IMAGE_TOPIC_R;
    fsSettings["imu_topic"] >> IMU_TOPIC;
    MAX_CNT = fsSettings["max_cnt"];
    MIN_DIST = fsSettings["min_dist"];
    FREQ = fsSettings["freq"];
    F_THRESHOLD = fsSettings["F_threshold"];
    STEREO_THRESHOLD = fsSettings["stereo_threshold"];
    MIN_TRACKED_CNT = fsSettings["min_tracked_cnt"];
    SHOW_TRACK = fsSettings["show_track"];
    EQUALIZE = fsSettings["equalize"];
    FISHEYE = fsSettings["fisheye"];
    if (FISHEYE == 1)
        FISHEYE_MASK = config_path + "/" + "fisheye_mask.jpg";
    CAM_NAMES.push_back(config_file);

    fsSettings["cam0_model"] >> CAM0_MODEL_PATH; 
    CAM0_MODEL_PATH = config_path + "/" + CAM0_MODEL_PATH;
    // ROS_INFO_STREAM("cam0_model:"<<CAM0_MODEL_PATH);

    fsSettings["cam1_model"] >> CAM1_MODEL_PATH;
    CAM1_MODEL_PATH = config_path + "/" + CAM1_MODEL_PATH;
    // ROS_INFO_STREAM("cam1_model:"<<CAM1_MODEL_PATH);

    cv::FileStorage cam0_model(CAM0_MODEL_PATH, cv::FileStorage::READ);
    cv::FileStorage cam1_model(CAM1_MODEL_PATH, cv::FileStorage::READ);

    cam0_model["image_height"] >> ROW;
    cam0_model["image_width"] >> COL;

    cam0_model["model"] >> CAM0_MODEL_NAME;
    cam0_model["intrinsics"] >> CAM0_MODEL_K;
    cam0_model["distortion"] >> CAM0_MODEL_D;
    cam0_model["imu_T_cam"] >> IMU_T_CAM0;

    cam1_model["model"] >> CAM1_MODEL_NAME;
    cam1_model["intrinsics"] >> CAM1_MODEL_K;
    cam1_model["distortion"] >> CAM1_MODEL_D;
    cam1_model["imu_T_cam"] >> IMU_T_CAM1;

    // ROS_INFO_STREAM("imu_T_cam0 : " << std::endl << IMU_T_CAM0);
    // ROS_INFO_STREAM("imu_T_cam1 : " << std::endl << IMU_T_CAM1);

    WINDOW_SIZE = 20; // this is only for display of the track quality
    STEREO_TRACK = false;
    FOCAL_LENGTH = (CAM0_MODEL_K.at<double>(0,0)+CAM0_MODEL_K.at<double>(0,1))/2.0;
    PUB_THIS_FRAME = false;

    if (FREQ == 0)
        FREQ = 100;

    fsSettings.release();


}
