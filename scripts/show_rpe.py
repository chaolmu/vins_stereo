import numpy as np
import matplotlib.pyplot as plt

def find_nearest(array,value):
    idx = np.abs(array-value).argmin()
    return idx

FONT_SIZE = 14
LINE_WIDTH = 1.5

DATA_SET = "V1_01_easy"
# DATA_SET = "V1_02_medium"
# DATA_SET = "V1_03_difficult"
# DATA_SET = "MH_01_easy"
# DATA_SET = "MH_02_easy"
# DATA_SET = "MH_03_medium"
# DATA_SET = "MH_04_difficult"
# DATA_SET = "MH_05_difficult"
# DATA_SET = "Ifa_lab"
# DATA_SET = "TUD_BAR2"


names = ['VINS-Stereo',  'VINS-Mono', 'VINS-Fusion','MSCKF']
paths = ['vins_stereo_rpe.csv', 'vins_mono_rpe.csv', 'vins_fusion_rpe.csv', 'msckf_rpe.csv']

# names = ['VINS-Stereo', 'VINS-Mono', 'MSCKF']
# paths = ['vins_stereo_rpe.csv', 'vins_mono_rpe.csv','msckf_rpe.csv']

# names = ['VINS-Stereo']
# paths = ['vins_stereo_imu_rpe.csv']

errors = []

for name,path in zip(names,paths):
    errors.append(np.genfromtxt(path, delimiter=' ',names=['stamp_est0', 'stamp_est1', 'stamp_gt0', 'stamp_gt1',
                        'trans_error', 'rot_error']))

start_time = errors[0]['stamp_gt0'][0]


fig = plt.figure("Relative Pose Error",figsize=(8,6))
plt.subplot(2,1,1)
print("Translational RMSE [m]:")
for name, error in zip(names, errors):
    stamps = error['stamp_gt0'] - start_time
    trans_error = error['trans_error']
    plt.plot(stamps, trans_error, label=name, linewidth=LINE_WIDTH)
    # trans_error = trans_error[1800:]
    # plt.axhline(y=np.sqrt(np.mean(trans_error**2)), color='r', linestyle='-')
    print(name + ": %.3f" % np.sqrt(np.mean(trans_error**2)))
plt.xlabel('t [s]', fontsize = FONT_SIZE)
plt.ylabel('translation error [m]', fontsize = FONT_SIZE)
plt.grid()
plt.ylim((0,0.2))
plt.legend(loc='best', shadow=False , fontsize = FONT_SIZE, ncol = 2)
plt.title('Relative Pose Error '+DATA_SET , fontsize = FONT_SIZE)

plt.subplot(2,1,2)
print("\nRotational RMSE [m]:")
for name, error in zip(names, errors):
    stamps = error['stamp_gt0'] - start_time
    rot_error = error['rot_error']*180.0 / np.pi
    plt.plot(stamps, rot_error, label=name, linewidth=LINE_WIDTH)
    rot_error = rot_error[find_nearest(stamps,8):]
    # print(stamps[80]-start_time)
    # plt.axhline(y=np.sqrt(np.mean(rot_error**2)), color='r', linestyle='-')
    print(name + ": %.3f" % np.sqrt(np.mean(rot_error**2)))
plt.xlabel('t [s]', fontsize = FONT_SIZE)
plt.ylabel('rotation error [deg]', fontsize = FONT_SIZE)
plt.ylim((0,3))
plt.grid()

plt.tight_layout()
fig.savefig(DATA_SET+"_RPE", dpi=300)

# plot box chart
data = [(x['trans_error'][find_nearest(x['stamp_gt0']-start_time,8):]).tolist() for x in errors ]
fig  = plt.figure("statistic",figsize=(6,4))
plt.subplot(2,1,1)
plt.title('Relative Pose Error', fontsize=FONT_SIZE)
plt.boxplot(data, patch_artist=True, showfliers=True, notch=False)
plt.ylabel('translation error [m]', fontsize=FONT_SIZE)
plt.xticks([1,2,3,4], names, fontsize=FONT_SIZE)

data = [((x['rot_error'][find_nearest(x['stamp_gt0']-start_time,8):])*180.0/np.pi).tolist() for x in errors ]
fig  = plt.figure("statistic",figsize=(6,4))
plt.subplot(2,1,2)
plt.boxplot(data, patch_artist=True, showfliers=True, notch=False)
plt.ylabel('rotation error [deg]', fontsize=FONT_SIZE)
plt.xticks([1,2,3,4], names, fontsize=FONT_SIZE)
fig.savefig(DATA_SET+"_RPE_Stat", dpi=300)