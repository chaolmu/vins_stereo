import numpy
import matplotlib.pyplot as plt
import transforms3d

def find_closest_index(L,t):
    """
    Find the index of the closest value in a list.
    
    Input:
    L -- the list
    t -- value to be found
    
    Output:
    index of the closest element
    """
    beginning = 0
    difference = abs(L[0] - t)
    best = 0
    end = len(L)
    while beginning < end:
        middle = int((end+beginning)/2)
        if abs(L[middle] - t) < difference:
            difference = abs(L[middle] - t)
            best = middle
        if t == L[middle]:
            return middle
        elif L[middle] > t:
            end = middle
        else:
            beginning = middle + 1
    return best

def transform44(l):
    """
    Generate a 4x4 homogeneous transformation matrix from a 3D point and unit quaternion.
    
    Input:
    l -- tuple consisting of (stamp,tx,ty,tz,qw,qx,qy,qz) where
         (tx,ty,tz) is the 3D position and (qw,qx,qy,qz) is the unit quaternion.
         
    Output:
    matrix -- 4x4 homogeneous transformation matrix
    """
    _EPS = numpy.finfo(float).eps * 4.0
    t = l[1:4]
    q = numpy.array(l[4:8], dtype=numpy.float64, copy=True)
    # q = numpy.array([l[5], l[6], l[7], l[4]], dtype=numpy.float64, copy=True)
    # nq = numpy.dot(q, q)
    # if nq < _EPS:
    #     return numpy.array((
    #     (                1.0,                 0.0,                 0.0, t[0])
    #     (                0.0,                 1.0,                 0.0, t[1])
    #     (                0.0,                 0.0,                 1.0, t[2])
    #     (                0.0,                 0.0,                 0.0, 1.0)
    #     ), dtype=numpy.float64)
    # q *= numpy.sqrt(2.0 / nq)
    # q = numpy.outer(q, q)
    # return numpy.array((
    #     (1.0-q[1, 1]-q[2, 2],     q[0, 1]-q[2, 3],     q[0, 2]+q[1, 3], t[0]),
    #     (    q[0, 1]+q[2, 3], 1.0-q[0, 0]-q[2, 2],     q[1, 2]-q[0, 3], t[1]),
    #     (    q[0, 2]-q[1, 3],     q[1, 2]+q[0, 3], 1.0-q[0, 0]-q[1, 1], t[2]),
    #     (                0.0,                 0.0,                 0.0, 1.0)
    #     ), dtype=numpy.float64)
    rot_mat = transforms3d.quaternions.quat2mat(q)
    return numpy.array((
        ( rot_mat[0,0],  rot_mat[0,1],   rot_mat[0,2], t[0]),
        ( rot_mat[1,0],  rot_mat[1,1],   rot_mat[1,2], t[1]),
        ( rot_mat[2,0],  rot_mat[2,1],   rot_mat[2,2], t[2]),
        (   0.0,  0.0,  0.0, 1.0)
        ), dtype=numpy.float64)

def read_trajectory(filename, matrix=True):
    """
    Read a trajectory from a text file. 
    
    Input:
    filename -- file to be read
    matrix -- convert poses to 4x4 matrices
    
    Output:
    dictionary of stamped 3D poses
    """
    file = open(filename)
    data = file.read()
    lines = data.replace(","," ").replace("\t"," ").split("\n") 
    list = [[float(v.strip()) for v in line.split(" ") if v.strip()!=""] for line in lines if len(line)>0 and line[0]!="#"]
    list_ok = []
    for i,l in enumerate(list):
        if l[4:8]==[0,0,0,0]:
            continue
        isnan = False
        for v in l:
            if numpy.isnan(v): 
                isnan = True
                break
        if isnan:
            sys.stderr.write("Warning: line %d of file '%s' has NaNs, skipping line\n"%(i,filename))
            continue
        list_ok.append(l)
    if matrix :
      traj = dict([(l[0]*1e-9,transform44(l[0:])) for l in list_ok])
    else:
      traj = dict([(l[0]*1e-9,l[1:8]) for l in list_ok])
    return traj

def transform_traj_left(traj, R, T):
    for stampe in traj:
        pose = traj.get(stampe)
        pose_rot = pose[0:3,0:3]
        pose_trans = pose[0:3,3]
        pose_rot = R@pose_rot
        pose_trans = R@pose_trans + T
        # print(pose_trans)
        # print(R.dot(pose_trans))
        traj.get(stampe)[0:3,0:3] = pose_rot
        traj.get(stampe)[0:3,3] = pose_trans
        # print(traj.get(stampe))
    return traj

def transform_traj_right(traj, R, T):
    for stampe in traj:
        pose = traj.get(stampe)
        pose_rot = pose[0:3,0:3]
        pose_trans = pose[0:3,3]
        pose_rot = pose_rot@R
        pose_trans = pose_rot@T + pose_trans
        # print(pose_trans)
        # print(R.dot(pose_trans))
        traj.get(stampe)[0:3,0:3] = pose_rot
        traj.get(stampe)[0:3,3] = pose_trans
        # print(traj.get(stampe))
    return traj


def evaluate_traj(traj_gt,traj_est):
    stamps_gt = list(traj_gt.keys())
    stamps_est = list(traj_est.keys())
    stamps_gt.sort()
    stamps_est.sort()

    index_est = list(traj_est.keys())
    index_est.sort()

    pairs = []
    for i in range(len(traj_est)):
        j = find_closest_index(stamps_gt,stamps_est[i])
        pairs.append((i,j))

    result = []
    for i,j in pairs:
        stamp_gt = stamps_gt[j]
        stamp_est = stamps_est[i]
        pose_gt = traj_gt[stamp_gt]
        pose_est = traj_est[stamp_est]

        # rot_gt = pose_gt[0:3,0:3]
        # trans_gt = pose_gt[0:3,3]
        # rot_est = pose_est[0:3,0:3]
        # trans_est = pose_est[0:3,3]

        T_error = numpy.linalg.inv(pose_gt)@pose_est

        # trans_error = rot_gt.transpose() @ (trans_est - trans_gt)
        # rot_error_mat = rot_gt.transpose() @ rot_est
        
        trans_error = T_error[0:3,3]
        rot_error_mat = T_error[0:3, 0:3]

        e_yaw, e_pitch, e_roll = transforms3d.euler.mat2euler(rot_error_mat, 'szyx')
        # e_yaw = numpy.abs(e_yaw)
        # e_pitch = numpy.abs(e_pitch)
        # e_roll = numpy.abs(e_roll)
        # print(e_yaw, e_pitch, e_roll)
        # print(trans_error)
        # trans_error = numpy.abs(trans_error)
        start_time = stamps_gt[0]
        result.append([stamp_est - start_time,trans_error[0],trans_error[1],trans_error[2],
                    e_yaw*180.0/numpy.pi,e_pitch*180.0/numpy.pi,e_roll*180.0/numpy.pi])

    return numpy.array(result)

#########################################################################

DATA_SET = "MH_01_easy"
names = ['VINS-Stereo', 'VINS-Mono', 'MSCKF', 'VINS-Fusion']
paths = ['vins_stereo_no_loop.csv', 'vins_mono.csv', 'msckf.csv', 'vins_fusion.csv']
# DATA_SET = "V1_01_easy"
# names = ['VINS-Stereo', 'VINS-Stereo-loop']
# paths = ['vins_stereo_no_loop.csv', 'vins_stereo_loop.csv']

traj_gt = read_trajectory('data.csv')
# traj_gt = read_trajectory('vins_stereo_loop.csv')
trajs_est = []
for path in paths:
    trajs_est.append(read_trajectory(path))

def align_gt(traj_gt, trag_est):
    stamps_gt = list(traj_gt.keys())
    stamps_est = list(trag_est.keys())
    stamps_gt.sort()
    stamps_est.sort()
    WG_T_G = traj_gt[stamps_gt[find_closest_index(stamps_gt,stamps_est[0])]]
    WB_T_B = trag_est[stamps_est[0]]
    ####################################################
    B_T_G = numpy.array([1.0, 0.0, 0.0,  7.48903e-02,
                            0.0, 1.0, 0.0, -1.84772e-02,
                            0.0, 0.0, 1.0, -1.20209e-01,
                            0.0, 0.0, 0.0,  1.0]).reshape((4,4))

    G_T_B = numpy.linalg.inv(B_T_G)
    WB_T_WG = WB_T_B @ B_T_G @ numpy.linalg.inv(WG_T_G)

    R = WB_T_WG[0:3,0:3]
    T = WB_T_WG[0:3,3]
    #####################################################
    traj_gt = transform_traj_left(traj_gt, R, T)
    R = G_T_B[0:3,0:3]
    T = G_T_B[0:3,3]
    traj_gt = transform_traj_right(traj_gt, R, T)

    # plt.figure(1)
    # time = []
    # x = []
    # y=[]
    # for stampe in stamps_gt:
    #     time.append(stampe)
    #     x.append(traj_gt[stampe][0,3])
    #     y.append(traj_gt[stampe][1,3])
    # plt.plot(x,y,label ='GT')
    # time = []
    # x = []
    # y=[]
    # for stampe in stamps_est:
    #     time.append(stampe)
    #     x.append(trag_est[stampe][0,3])
    #     y.append(trag_est[stampe][1,3])
    # plt.plot(x,y,label ='EST')
    # plt.axis('equal')
    # plt.legend()
    # plt.show()
    return traj_gt



################################################################
results = []
for traj in trajs_est:
    traj_gt_aligned = align_gt(traj_gt, traj)
    results.append(evaluate_traj(traj_gt_aligned, traj))

fig = plt.figure("Absolute Pose Error",figsize=(8,6))
plt.subplot(3,1,1)
for result, i in zip(results, range(len(results))):
    time = result[:,0]
    error = result[:,1]
    plt.plot(time, error, label = names[i])
    plt.ylabel('x error [m]')
    
plt.grid()
plt.title('Absolute Translation Error '+ DATA_SET)
plt.legend(loc='best', shadow=False, ncol=4)

plt.subplot(3,1,2)
for result, i in zip(results, range(len(results))):
    time = result[:,0]
    error = result[:,2]
    plt.plot(time, error)
    plt.ylabel('y error [m]')
plt.grid()

plt.subplot(3,1,3)
for result, i in zip(results, range(len(results))):
    time = result[:,0]
    error = result[:,3]
    plt.plot(time, error)
    plt.ylabel('z error [m]')
plt.grid()
# plt.legend(loc='best', shadow=False)

plt.xlabel('t [s]')
plt.tight_layout()
fig.savefig(DATA_SET+"_APE_trans", dpi=300)
plt.close()

####################################################################

fig = plt.figure("Absolute Pose Error",figsize=(8,6))
plt.subplot(3,1,1)
for result, i in zip(results, range(len(results))):
    time = result[:,0]
    error = result[:,4]
    plt.plot(time, error)
    plt.ylabel('yaw error [deg]')
plt.grid()

plt.title('Absolute Rotation Error '+DATA_SET)
# plt.legend(loc='best', shadow=False, ncol=4)

plt.subplot(3,1,2)
for result, i in zip(results, range(len(results))):
    time = result[:,0]
    error = result[:,5]
    plt.plot(time, error)
    plt.ylabel('pitch error [deg]')
plt.grid()

plt.subplot(3,1,3)
for result, i in zip(results, range(len(results))):
    time = result[:,0]
    error = result[:,6]
    plt.plot(time, error, label = names[i])
    plt.ylabel('roll error [deg]')
plt.grid()
plt.legend(loc='best', shadow=False, ncol=4)

plt.xlabel('t [s]')
plt.tight_layout()
fig.savefig(DATA_SET+"_APE_rot", dpi=300)
plt.close()

