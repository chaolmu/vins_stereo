import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

data = np.genfromtxt("motion.txt", delimiter=',', skip_header=1, names=['seq', 'frame_id', 'timestamp', 'accel_x', 'accel_y', 'accel_z', 'gyro_x', 'gyro_y', 'gyro_z', 'temperature'])
time = (data['timestamp'] - data['timestamp'][0])/1e5
acc_x = data['accel_x']*9.81
acc_y = data['accel_y']*9.81
acc_z = data['accel_z']*9.81

gyro_x = data['gyro_x']
gyro_y = data['gyro_y']
gyro_z = data['gyro_z']


fig  = plt.figure("acc",figsize=(8,6))
plt.subplot(3,1,1)
plt.plot(time, acc_x)
plt.ylabel('acc x [m/s$^2$]', fontsize=14)
plt.title('Acceleration', fontsize=14)
plt.grid()
plt.subplot(3,1,2)
plt.plot(time, acc_y)
plt.ylabel('acc y [m/s$^2$]', fontsize=14)
plt.grid()
plt.subplot(3,1,3)
plt.plot(time, acc_z)
plt.ylabel('acc z [m/s$^2$]', fontsize=14)
plt.grid()
plt.xlabel('time [s]', fontsize=14)
fig.savefig("acc_stat", dpi=300)

fig  = plt.figure("gyro",figsize=(8,6))
plt.subplot(3,1,1)
plt.plot(time, gyro_x)
plt.ylabel('gyro x [deg/s]', fontsize=14)
plt.title('Angular velocity', fontsize=14)
plt.grid()
plt.subplot(3,1,2)
plt.plot(time, gyro_y)
plt.ylabel('gyro y [deg/s]', fontsize=14)
plt.grid()
plt.subplot(3,1,3)
plt.plot(time, gyro_z)
plt.ylabel('gyro z [deg/s]', fontsize=14)
plt.grid()
plt.xlabel('time [s]', fontsize=14)
fig.savefig("gyro_stat", dpi=300)



